import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EsporteMidiaTeste {


    public static void main(String[] args){

        //String lineContainingChannels = "ESPN";
        //String lineContainingChannels = "Globo (RJ, PA, PE, RR, RO, AC, AM, MA, PI, RN, PB, BA, ES, SC, DF e Juiz de Fora) e sportv3";
         //String lineContainingChannels = "Record (RJ, ES, MG, GO, TO, DF, MA, PI, CE, RN, PB, PE, AL, SE, BA, PA, AM, AP, RR e AC), Twitch e Cariocão Play (operadoras, YouTube, Globo e clubes)";
        //String lineContainingChannels = "Sportv (MG e SP), Band (SP, MG e ES) e ESPN";
         //String lineContainingChannels = "Sportv e Globo";
         //String lineContainingChannels = "Sportv, Record e Globo";
         // String lineContainingChannels = "Sportv, Globo e ESPN (SP, MG e RJ)";        //NEW
         String lineContainingChannels = "Sportv (SP, BA, RS), Record, Globo (SP, RJ e MG) e ESPN (SP, MG e RJ)";
       // String lineContainingChannels = "Sportv (RJ e MG), Record, Globo (SP, RJ e MG) e ESPN (SP, MG e RJ)";
       // String lineContainingChannels = "Sportv (MG e SP), Band (SP e ES) e ESPN";
        //String lineContainingChannels = "Sportv (MG,RJ e SP), Band (SP e ES) e ESPN (SP e RJ)";
        //String lineContainingChannels = "Sportv e Band (MG e SP)";

        int andOcurrence = countMatches(lineContainingChannels, " e ");
        int commaOcurrence = countMatches(lineContainingChannels, ",");
        int parenthesesOcurrence = countMatches(lineContainingChannels, "\\(");

        if(parenthesesOcurrence > 0) {
            //tokeniza os parenteses
            lineContainingChannels = tokenizeParentheses(lineContainingChannels, parenthesesOcurrence);
        }

        List<String> channels = new ArrayList<>();

        if (StringUtils.containsAny(lineContainingChannels, ",") || StringUtils.containsAny(lineContainingChannels, " e ")) {
              for (int i = 0; i < commaOcurrence; i++) {
                    //se "," está entre parenteses, substitua por "_"
                    if (checkCommaBetweenParentheses(lineContainingChannels)) {
                        final int positionComma = StringUtils.indexOf(lineContainingChannels, ",");
                        String newLine = lineContainingChannels;
                        lineContainingChannels = newLine.substring(0, positionComma) + "_" + newLine.substring(positionComma + 1);
                    } else {
                        //se está fora, substitua por "|"
                        final int positionComma = StringUtils.indexOf(lineContainingChannels, ",");
                        String newLine = lineContainingChannels;
                        lineContainingChannels = newLine.substring(0, positionComma) + "|" + newLine.substring(positionComma + 2);
                    }
                }

              for (int i = 0; i < andOcurrence; i++) {
                  //se " e " está entre parenteses, substitua por "$"
                    if (checkAndBetweenParentheses(lineContainingChannels)) {
                        final int positionAnd = StringUtils.indexOf(lineContainingChannels, " e ");
                        String newLine = lineContainingChannels;
                        lineContainingChannels = newLine.substring(0, positionAnd) + " $" + newLine.substring(positionAnd + 2);
                    } else {
                        //se está fora, substitua por "|"
                        final int positionComma = StringUtils.indexOf(lineContainingChannels, " e ");
                        String newLine = lineContainingChannels;
                        lineContainingChannels = newLine.substring(0, positionComma) + "|" + newLine.substring(positionComma + 3);
                    }
                }

               if(parenthesesOcurrence > 0) {
                   //desfaz as alterações nos caractéres entre parênteses
                   lineContainingChannels = untokenizeAll(lineContainingChannels);
               }

            channels = Arrays.asList(StringUtils.split(lineContainingChannels, "|"));

        } else {
            channels.add(lineContainingChannels);
        }

        System.out.println(channels);

    }

    static private boolean checkCommaBetweenParentheses(String line) {
        String[] token = {"[","]","{","}","(",")"};
        for(int i = 0; i <=4; i++ ) {
            if (StringUtils.containsAny(line, token[i]) && StringUtils.containsAny(line, token[i+1])) {
                final int commaPosition = StringUtils.indexOf(line, ",");
                final int positionOpen = StringUtils.indexOf(line, token[i]);
                final int positionClose = StringUtils.indexOf(line, token[i+1]);
                if ((positionOpen < commaPosition) && (positionClose > commaPosition)) {
                    return true;
                }
                i++;
            }
        }
        return false;
    }

    static private boolean checkAndBetweenParentheses(String line) {
        String[] token = {"[","]","{","}","(",")"};
        for(int i = 0; i <=4; i++ ) {
            if (StringUtils.containsAny(line, token[i]) && StringUtils.containsAny(line, token[i+1])) {
                final int positionAnd = StringUtils.indexOf(line, " e ");
                final int positionOpen = StringUtils.indexOf(line, token[i]);
                final int positionClose = StringUtils.indexOf(line, token[i+1]);
                if ((positionOpen < positionAnd) && (positionClose > positionAnd)) {
                    return true;
                }
                i++;
            }
        }
        return false;
    }

    // Checks if a string is empty ("") or null.
    public static boolean isEmpty(String s) {
        return s == null || s.length() == 0;
    }

    public static int countMatches(String text, String str)
    {
        if (isEmpty(text) || isEmpty(str)) {
            return 0;
        }
        return text.split(str, -1).length - 1;
    }

    public static String tokenizeParentheses(String line, int parenthesesOcurrence) {

        String[] token = {"}","{","]","["};

        if(StringUtils.containsAny(line, "(")) {
            for (int i = (parenthesesOcurrence * 2) - 3; i >= 0; i--) {
                int positionOpen = StringUtils.indexOf(line, "(");
                String newLine = line;
                line = newLine.substring(0, positionOpen) + token[i] + newLine.substring(positionOpen + 1);
                i--;
                int positionClose = StringUtils.indexOf(line, ")");
                newLine = line;
                line = newLine.substring(0, positionClose) + token[i] + newLine.substring(positionClose + 1);
            }
        }
        return line;
    }

    public static String untokenizeAll(String line) {
        if (StringUtils.containsAny(line, "_")) {
            line = line.replace("_", ",");
        }
        if (StringUtils.containsAny(line, "$")) {
            line = line.replace("$", " e ");
        }
        if (StringUtils.containsAny(line, "[")) {
            line = line.replace("[", "(");
            line = line.replace("]", ")");
        }
        if (StringUtils.containsAny(line, "{")) {
            line = line.replace("{", "(");
            line = line.replace("}", ")");
        }
        return line;
    }

}
